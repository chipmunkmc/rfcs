# RFC #0000 - Genesis RFC

## Preface

This RFC defines the structure of following RFCs, as well as itself, and the way this Git repository is supposed to be used.

### Terminology

A "Proposed RFC" is a pull request made to the main repository in the format this document specifies.
A "Frozen RFC" is an RFC that has been merged into main.
A "Melted RFC" is an RFC that has been dropped as a frozen standard.

### Definition

A Request For Comment (or RFC for short) must be proposed via a pull request to this repository. 
It must be a valid markdown file, and the contained Markdown file must contain a header like this one, and follow the rules outlined in this document.

### File Name

File names must include the left-padded RFC number along with the title, where the RFC and title are split with a dash.
Spaces in the title must be replaced with an undescore.
Maintain the casing of the in-file title.
The extension of the filename must be `md` with no variation in casing.

For example, the file name of this RFC must be:

```
0000-Genesis_RFC.md
```

## File Format

### Titling

RFCs must not be titled with special characters or punctuation of any kind. 
Stick to the English alphabet, and split words with an ASCII space.

### Header

For example, if I were to create a RFC for "Namespaced Prefixes", and the previously proposed RFC (this includes pending pull requests) was 1233, the header would be:

```
# RFC #1234 - Namespaced Prefixes
```

### Sectioning

You must attempt to break the contents of the RFC into sections.
Sections must begin with the `##` tag, whereas sub-sections must begin with the `###` tag.
Sub-sub-sections should be kept to a minimum, however if they are needed they must begin with the `####` tag.

Do not create any sections with the `#` tag, as that is reserved for the header.
Sections must be title-case. Exclusively uppercase and lowercase casing must not be used.

You should start your document with a section named "Preface", where you discuss basic principles of the idea and why you are suggesting it as a standard.
Explain it in more detail in other sections.
In your preface section, you can define sub-sections, but they should also be short.

### Tags

No formatting, except for code-blocks, headers and links is allowed. 
You must not embed images.

### Sentences & Wording

Sentences smust be split with a line break, and you must not split up sentences for a consistent line size.
You can split up a group of sentences about similar ideas into paragraphs with two newlines.

You must only use proper grammar, proper casing, proper casing and proper punctuation.

### Language

You must not use expletives (swearing) in RFCs.
You must keep the use of informal language to a minimum.

Hard requirements must be explicitly stated with the use of "must" or "must not".
Soft requirements must be explicitly stated with the use of "should" or "should not".

Please try to keep the use of complex words to a minimum. 
Try to only use simple language, as the Kaboom community has a large amount of non-native English speakers with varying levels of proficiency who may want to participate in this project too and must be denied from doing so by something like a language barrier.  
However, you must not choose simple words over complex counterparts when meaning will be lost.
You must use long words if shorter words are unable to convey your meaning.

## Comments, Changes & Propositions

### RFC Numbering

When you make your RFC, you must increment RFC number of the most recently proposed/frozen RFC by one.
The numbers used by RFCs that are denied in the proposal stage and melted RFCs will never be freed for usage by another RFC to avoid confusion and proposed RFC update cascades.

### Comments

Please ensure that you keep discussion around a RFC on-topic and civil. Disrespect and off-topic discussion will lead to your removal, or blocking, from the organization.

### Changes

Feel free to suggest changes to a proposed RFC in the form of pull request reviews. Changes requested by organization owners will block the merging of the PR into the main branch, thus preventing it from becoming a frozen standard.

### Freezing Proposed RFCs

A proposed RFC must only be frozen (merged into the main branch) once a consensus among the organization members and general community consensus has been reached.

### Melting RFCs

A frozen RFC must be melted (dropped) only via creating an issue on this repository, indicating your intent in the title and explaining the reasoning behind why you want the RFC to be removed.
The frozen RFC must become melted only when a organization consensus and a general community consensus has been reached.
This RFC may not be melted, it must only be modified and superseded.

### Modifying Existing RFCs

Existing RFCs may be modified in place to fix grammar, spelling and improve wording.
However, any significant change to the ideas behind the writing should be made in the form of another RFC which will supersede the previously frozen RFC.

When you are making an RFC that supersedes another, commit your new RFC and in a separate commit add a disclaimer `#` header to the top of the superseded RFC's file.

#### Disclaimer Header Format

If the RFC being superseded was #1234 - SelfCare and the RFC superseding it was #4321 - Exponential Backoff SelfCare, the disclaimer would be formatted like this:

```
# THIS RFC HAS BEEN SUPERSEDED BY [#4321](./4321-Exponential_Backoff_SelfCare.md), AND IS ONLY PROVIDED FOR HISTORICAL PURPOSES!
```

### Commits

Commits must be prefixed with the RFC they are modifying, e.g. `RFC 0000`, and a short description of their purpose.
For example, if I were adding this section to this RFC, my commit would be:

```
RFC 0000: Add section on commits
```

You may provide a more detailed explanation in the commit description, however this should not be done for small changes.

## Licensing

RFCs, including this one, are licensed under [The Unlicense](https://unlicense.org) in both the proposal and frozen stage.
RFCs which define another license must not be accepted until their license is changed to match The Unlicense.
